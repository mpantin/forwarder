import requests
import time
import sentry_sdk
from forwarder.models import RequestsLog


def forward_data(payload: dict, path: str, method: str, i: int = 0, request_log_id: int = None):
    if i >= 5:
        return
    method = method.upper()
    if not path.startswith('https://marktim.shop/'):
        path = f'https://marktim.shop/{path}' if not path.startswith('/') else f'https://marktim.shop{path}'
    # path = f'https://marktim.ngrok.io/{path}' if not path.startswith('/') else f'https://marktim.ngrok.io{path}'
    request_log = None
    if request_log_id:
        request_log = RequestsLog.objects.filter(id=request_log_id).first()
    if not request_log:
        request_log = RequestsLog.objects.create(request_url=path, request_method=method, payload=payload, success=False)
    try:
        if method == 'GET':
            response = requests.get(path, verify=False, timeout=100)
        elif method == 'POST':
            response = requests.post(path, json=payload, verify=False, timeout=100)
        elif method == 'PUT':
            response = requests.put(path, json=payload, verify=False, timeout=100)
        elif method == 'DELETE':
            response = requests.delete(path, verify=False, timeout=100)
        else:
            return
        status_code = response.status_code
        request_log.response_code = int(status_code)
        if status_code in {200, 201, 202, 203, 204}:
            request_log.success = True
        request_log.save()
        return True
    except Exception as e:
        i += 1
        time.sleep(2)
        result = forward_data(payload, path, method, i, request_log.id)
        if result:
            return
        sentry_sdk.capture_exception(e)
        request_log.success = False
        request_log.save()
