from django.db import models


class RequestsLog(models.Model):
    request_url = models.CharField(max_length=255, default='')
    request_method = models.CharField(max_length=10, default='')
    request_timestamp = models.DateTimeField(auto_now_add=True)
    response_code = models.IntegerField(default=None, null=True)
    payload = models.JSONField(default=dict)
    success = models.BooleanField(default=False)

    class Meta:
        db_table = 'requests_log'

    def __str__(self):
        return f'{self.id} - {self.request_method} - {self.request_url}'
