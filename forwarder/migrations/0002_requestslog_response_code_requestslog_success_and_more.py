# Generated by Django 4.2.8 on 2023-12-07 22:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forwarder', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='requestslog',
            name='response_code',
            field=models.IntegerField(default=None, null=True),
        ),
        migrations.AddField(
            model_name='requestslog',
            name='success',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='requestslog',
            name='request_method',
            field=models.CharField(default='', max_length=10),
        ),
        migrations.AlterField(
            model_name='requestslog',
            name='request_url',
            field=models.CharField(default='', max_length=255),
        ),
    ]
