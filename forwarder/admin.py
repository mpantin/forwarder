from django.contrib import admin
from forwarder.models import RequestsLog


@admin.register(RequestsLog)
class RequestsLogAdmin(admin.ModelAdmin):
    list_display = ('id', 'request_method', 'request_url', 'response_code', 'request_timestamp')
    list_filter = ('request_method', 'response_code')
    search_fields = ('request_url',)
