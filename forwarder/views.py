from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json
from threading import Thread
from .utils import forward_data


@csrf_exempt
def request_forward(request, *args, **kwargs):
    json_data = dict()
    if request.method in {'POST', 'PUT'}:
        json_data = json.loads(request.body)
    thread = Thread(target=forward_data, args=(json_data, request.get_full_path(), request.method))
    thread.start()
    return HttpResponse('OK')
