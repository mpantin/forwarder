# General info
This project is intended to work as webhook forwarder due to unknown issue, probably related to discarding traffic 
between Russia and some other countries, that causes problems of webhook event delivery.

The following manual would be describing process of deployment of this project to `Ubuntu Server`.

The project would be run using the special unprivileged user `forwarder` of `www-data` group

## How to deploy
This repository is relaying to following components: 
1. `python3` - tested with python 3.11 (could be installed through third party PPA)
2. `certbot` - Utility to issue https certificate
3. `nginx`
4. `systemd`
5. Some python packages, that are listed in `requirements.txt`

Before deployment, you may want to adjust some settings inside the project and config files.

All configuration files are located inside `config` folder

You need to set your domain name in `forwarder.conf`

Also, you need to add your web address to `CORS_ORIGIN_WHITELIST` and 
`CSRF_TRUSTED_ORIGINS` lists in `config.py`

If you prefer to use your own folder names, don\`t forget to adjust path to a project
in following files: `forwarder.conf`, `forwarder.ini`, `forwarder.service`. 

In case you chose your own log directory, you need to fix `logrotate` config in file called `uwsgi`

You may also want to use more secure endpoint (adding some random trailer) to receive webhooks it could be 
configured maintaining a string in `urls.py`

### Update system packages

`sudo apt update && sudo apt upgrade -y && sudo apt autoremove -y`

### Install following components

`sudo apt install -y nginx build-essential certbot python3-certbot-nginx git`

### Install python 3.11

`sudo add-apt-repository ppa:deadsnakes/ppa -y`

`sudo apt update`

`sudo apt install python3-pip python3-venv python3.11-dev python3.11-venv`

### Create infrastructure for running the project
Create user, add it to `www-data` group and create folders for internal logs

`sudo adduser forwarder` - at this step enter password for new user, we will use it later

`sudo usermod -aG www-data forwarder`

`sudo mkdir -p /var/log/uwsgi/`

`sudo chown -R forwarder:www-data /var/log/uwsgi/`

### Login as forwarder
`sudo su - forwarder`

**Being logged in as a forwarder**

`mkdir -p Projects && cd ./Projects/`

`python3.11 -m venv env`

`source ./env/bin/activate`

Clone repo:

`git clone https://gitlab.com/mpantin/forwarder.git`

`pip install -r ./forwarder/requirements.txt`

`./forwarder/manage.py migrate`

`./forwarder/manage.py collectstatic --no-input`

`./forwarder/manage.py createsuperuser` - complete creation of user to admin panel using hints

**Log out from `forwarder`** - if you are following this guide, just execute `exit`

### Copy configuration files to appropriate folders and run project

**Being logged in as a sudo user**

`sudo cp /home/forwarder/Projects/forwarder/config/forwarder.conf /etc/nginx/sites-available/`

`sudo ln -s /etc/nginx/sites-available/forwarder.conf /etc/nginx/sites-enabled/`

`sudo rm -rf /etc/nginx/sites-enabled/default`

`sudo cp /home/forwarder/Projects/forwarder/config/forwarder.service /etc/systemd/system/`

`sudo systemctl enable forwarder.service`

`sudo systemctl restart nginx forwarder`

`sudo certbot --nginx -d` *<domain_name>*

`sudo cp /home/forwarder/Projects/forwarder/config/uwsgi /etc/logrotate.d/`

